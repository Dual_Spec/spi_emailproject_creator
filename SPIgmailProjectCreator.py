from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']



def main():
    gizmo = 'Label_4550426910855382973'
    files = 'Label_6433850562226586380'
    messages = getLabeledEmails(generateCreds(), files)

def generateCreds():
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)
    return service


def getLabeledEmails(service, label_text):
    try:
        #Debug command to find label ids
        #print(service.users().labels().list(userId='me').execute())

        user_id = 'me'
        label_ids = [label_text]
        response = service.users().messages().list(userId=user_id,labelIds=label_ids).execute()
        messages = []
        
        if 'messages' in response:
            messages.extend(response['messages'])
            print(messages)

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id, labelIds=label_ids, pageToken=page_token).execute()
            messages.extend(response['messages'])
            print(messages)
        return messages
    except errors.HttpError as error:
        print(error)

'''
        # Call the Gmail API
    results = service.users().labels().list(userId='me').execute()
    labels = results.get('labels', [])

    if not labels:
        print('No labels found.')
    else:
        print('Labels:')
        for label in labels:
            print(label['name'])
    #ListMessagesWithLabels('gmail','me', 'gizmo')

'''



if __name__ == '__main__':
    main()

